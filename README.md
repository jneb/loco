# README #

This program is for users (having children) that like to play loco or miniloco:
see https://www.minilocoshop.nl/alle-producten/maxi-loco.
This program is written in Dutch; this is also the case for that product, by the way.
Loco allows to playfully test your skill at school by answering a set of 12 or 24 questions,
and answer by placing blocks in a grid. If you do it right, you'll get a nice pattern if you 
flip over the grid.
This program allows to generate questions and answers.

### What is this repository for? ###

* Generate questions and answers on a set of different subjects: give "loco ?" for a list.
* The self test didn't work since porting to Python3; to be fixed
* give "loco <subject>" to generate a few sets of questions and answers

### How do I get set up? ###

* It is just a single file

### Contribution guidelines ###

* feel free to contact me at jnebos on the google mail server