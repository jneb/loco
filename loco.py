#!/usr/bin/env python3
# vim:fileencoding=utf-8
'''(Mini)loco generator.

Kan kant en klare Loco sommen en oplossingen produceren.
'''

from random import randrange, shuffle, choice, seed
from operator import itemgetter, attrgetter
from bisect import bisect

# de symbolen op de achterkant van de blokjes
BLOKJE_SYMBOLEN = r'\G\BR/G/r\B/b\g\\R/r/g/bg/b/r/\g/B\rB\/G/R\bG\R\\'
MINIBLOKJE_SYMBOLEN = r'\G\BR/G/R\B/G\/B\R/G/RB\\'


class Blokje:
    '''Een blokje van het loco spel.
    Bevat ook de variabelen sym2num en symlist voor het omrekenen van blokjes.
    Blokje wordt aangegeven door een ruwe tekening:
    hoofdletter is kleur; kleine letter is juist geen kleur.
    '''
    #Bereken de tabellen voor het omdraaien van de stukjes.
    symlist = [BLOKJE_SYMBOLEN[i:i+2] for i in range(0,48,2)]
    sym2num = dict(list(zip(symlist, list(range(1,25)))))

    def __init__(self, id):
        '''Per cijfer, per symbool, of per eigenschappen.
        >>> Blokje('r/')
        Blokje(15)
        >>> Blokje(15).symbool()
        'r/'
        >>> Blokje('z/')
        Traceback (most recent call last):
            [...]
        ValueError: Kan geen blokje z/ maken
        >>> Blokje(('/', 0, True, 'G')).symbool()
        '/G'
        '''
        #eenvoudige gevallen
        if isinstance(id, int) and 0<=id<=24:
            self.num = id
            return
        elif id is None:
            self.num = 0
            return
        elif isinstance(id, (Blokje, MiniBlokje)):
            self.num = id.num
            return
        #maak vanaf de achterkant
        if isinstance(id, tuple):
            hoek, pos, vol, kleur = id
            if kleur not in 'RGB': raise ValueError('Blokje: kleur niet RGB')
            if not vol: kleur = kleur.lower()
            if pos: id = kleur+hoek
            else: id = hoek+kleur
        try:
            self.num = self.sym2num[id]
        except KeyError: raise ValueError('Kan geen blokje %s maken'%id)

    @staticmethod
    def numVanEigenschappen(xxx_todo_changeme):
        (hoek, pos, kleur, vol) = xxx_todo_changeme
        if kleur not in 'RGB': raise ValueError('Blokje: kleur niet RGB')
        if not vol: kleur = kleur.lower()
        if pos: return kleur+hoek
        else: return hoek+kleur

    def symbool(self):
        return self.symlist[self.num-1]

    def eigenschappen(self):
        '''Geef terug de vier eigenschappen van de achterkant:
            hoek: \ of /
            pos: plaats van de grenslijn: 0=links, 1=rechts
            vol: False=een kwart gekleurd; True=driekwart gekleurd
            kleur: R, G of B
        >>> Blokje(13).eigenschappen()
        ('/', 1, False, 'G')
        '''
        symbolCode = self.symlist[self.num-1]
        hoek = '/' in symbolCode and '/' or '\\'
        pos = symbolCode.index(hoek)
        kleur = symbolCode[1-pos].upper()
        vol = kleur in symbolCode
        return hoek, pos, vol, kleur

    def achterkant(self):
        '''Geef de vier regels om het blokje mooi groot af te drukken.
        >>> Blokje(3).achterkant()
        ('RRR/', 'RR/ ')
        '''
        if not self.num: return ' '*4, ' '*4
        hoek, pos, vol, kleur = self.eigenschappen()
        #zet kleur aan goede kant
        if vol==pos: regel = 3*kleur+hoek+3*' '
        else: regel = 3*' '+hoek+3*kleur
        #zet grens aan goede kant
        if pos==0: regel = regel[2:]
        if hoek == '/': return regel[0:4], regel[1:5]
        else: return regel[1:5], regel[0:4]

    def __repr__(self): return 'Blokje(%d)'%self.num

    def __hash__(self):
        '''Maak blokjes hashbaar.'''
        return hash(self.num)

    def linkerkantKleur(self):
        """Geef aan of de linkerkant van het blokje is gekleurd.
        >>> Blokje(3).linkerkantKleur()
        1
        """
        hoek, pos, vol, kleur = self.eigenschappen()
        return vol==pos

    def onderkantKleur(self, bovenkant=False):
        """Geef aan welk gedeelte van de onderkant is gekleurd.
        Bit 0 is rechts en bit 1 is links.
        Als B1 boven B2, dan zit er wit tussen als
        B2.bovenkantKleur & B1.onderkantKleur == 0
        >>> [i for i in range(24) if Blokje(i).onderkantKleur()==0]
        [5, 7, 8, 10, 11, 12]
        >>> [i for i in range(24) if Blokje(i).onderkantKleur()==1]
        [1, 2, 9, 13, 14, 15]
        >>> [i for i in range(24) if Blokje(i).onderkantKleur()==2]
        [3, 4, 6, 16, 18, 22]
        >>> [i for i in range(24) if Blokje(i).onderkantKleur()==3]
        [0, 17, 19, 20, 21, 23]

        0: wit onder,   als leeg en /. of .\
        1: kleur links, als leeg en \. of vol en ./
        2: kleur rechts,als leeg en ./ of vol en \.
        3: kleur onder, als vol en /. of .\
        """
        hoek, pos, vol, kleur = self.eigenschappen()
        middenGrens = '\\/'[pos]==hoek
        linkerkantKleur = vol==pos
        if middenGrens^bovenkant: return 1+linkerkantKleur
        else: return 3*vol

    def bovenkantKleur(self):
        """Bovenkant.
        Om overduidelijke redenen wordt hier gebruik gemaakt van de
        onderkantroutine.
        >>> [i for i in range(24) if Blokje(i).bovenkantKleur()==0]
        [13, 14, 15, 16, 18, 22]
        >>> [i for i in range(24) if Blokje(i).bovenkantKleur()==1]
        [5, 7, 8, 17, 20, 21]
        >>> [i for i in range(24) if Blokje(i).bovenkantKleur()==2]
        [0, 10, 11, 12, 19, 23]
        >>> [i for i in range(24) if Blokje(i).bovenkantKleur()==3]
        [1, 2, 3, 4, 6, 9]
        """
        return self.onderkantKleur(bovenkant=True)


class MiniBlokje(Blokje):
    '''Een blokje van het miniloco spel.
    >>> MiniBlokje(3).achterkant()
    ('RRR/', 'RR/ ')
    >>> MiniBlokje(11).eigenschappen()
    ('/', 0, True, 'R')
    >>> MiniBlokje(10).achterkant()
    (' /GG', '/GGG')
    >>> Blokje(10).achterkant()
    ('R/  ', '/   ')
    '''
    #nieuwe omrekentabellen
    symlist = [MINIBLOKJE_SYMBOLEN[i:i+2] for i in range(0,24,2)]
    sym2num = dict(list(zip(symlist, list(range(1,13)))))

    def __init__(self, id):
        '''Constructor maakt gebruik van Blokje.
        >>> MiniBlokje('G\\\\')
        MiniBlokje(7)
        >>> MiniBlokje(7).symbool()
        'G\\\\'
        >>> MiniBlokje('z/')
        Traceback (most recent call last):
            [...]
        ValueError: Kan geen blokje z/ maken
        >>> MiniBlokje(('/', 0, True, 'G')).symbool()
        '/G'
        >>> MiniBlokje(17)
        Traceback (most recent call last):
            [...]
        ValueError: MiniLoco heeft blokjes 1-12
        >>> MiniBlokje(('/', 0, False, 'G'))
        Traceback (most recent call last):
            [...]
        ValueError: Minilocoblokjes zijn altijd vol
        '''
        if isinstance(id, int) and id>12: raise ValueError('MiniLoco heeft blokjes 1-12')
        if isinstance(id, tuple) and id[2]!=True: raise ValueError('Minilocoblokjes zijn altijd vol')
        Blokje.__init__(self, id)

    def __repr__(self): return 'MiniBlokje(%d)'%self.num

class FigurenSet:
    '''Een aantal gegenereerde figuren.
    '''
    def __init__(self, *namen):
        """Genereer een figurenset door de gegeven sets samen te voegen.
        >>> a=FigurenSet('lijnsymm')
        """
        self.verzameling= []
        for naam in namen:
            self.verzameling.extend(getattr(FigurenSet, 'f'+naam.title()))

    fLijnsymm = [
        ['*', 'inverse', 'down2'], #ster en bootjes
        ['ruit', 'down2', 'inverse'], #ruit en zandloper
        ['V', 'ud', 'inverse'], #V, ^
        ['kaars', 'swap', 'ud', 'inverse'], #beide kaarsen, ook op z'n kop
        ['dubbelruit', 'ud'], #dubbelruit, boven en onder
        ['miniruit'], #miniruit (inverse heeft verkeerde zijkant)
        ]
    fPuntsymm = [
        ['*', 'down2', 'inverse'], #ster en bootjes
        ['S', 'lr', 'inverse'], #S met spiegelbeeld
        ['..','ud', 'inverse'], #twee puntjes schuin boven elkaar
        ['><', 'ud', 'inverse'], #> links boven < en < rechts boven >
        ]
    fKlein = [
        ['*'],
        ['ruit', 'down2'], #ruit en zandloper
        ['>', 'lr'], #> en <
        ['peer', 'lr'], #peer met spiegelbeeld
        ['kaars', 'swap', 'ud'], #beide kaarsen, ook op z'n kop
        ['..', 'ud'], #twee puntjes schuin boven elkaar
        ]
    fMiddel1 = [
        ['*', 'inverse', 'down2'], #ster en bootjes
        ['ruit', 'down2', 'inverse'], #ruit, zandloper, inverses (=verschoven)
        ['V', 'ud', 'down2'], #V en ^ en inverse
        ]
    fMiddel2 = [
        ['>', 'lr', 'right2', 'switch'], #>, <, inverse, en "verkeersbord"
        ['S', 'switch', 'lr'], #S en ><
        ['>>', 'lr', 'down2', 'inverse'], #versprongen >>, <<, andersom, inverse
        ['..', 'down2', 'inverse'],
        ['>.', 'lr', 'down2', 'inverse'],
        ]
    fGroot = [
        ['peer', 'lr', 'down', 'down2'], #peer, op z'n kop en twee rare zandlopers
        ['dubbelruit', 'ud', 'right2'],
        ['miniruit', 'right2', 'down', 'down2'],
            #down geeft soort open kaars, down2 geeft bolletjesslinger
        ['kaars', 'swap', 'ud'],
        ]
    fShift = [
        ['>', 'switch', 'down', 'down2', 'right', 'right2'], #S, verkeersbord
        ['>>', 'right3', 'lr', 'down2'], #right2 geeft inverse
        ['..', 'down2', 'right', 'right2'],
        ['><', 'down2', 'right', 'right2'],
        ['>.', 'lr', 'down2', 'right', 'right2'],
        ]
    fMini = [
        ['bol', 'down', 'right2'], #bol, dikke zandloper, gordijn, ruit
        ['v', 'right2', 'down'], #v, ^, bakje, torentje
        ['{', 'down', 'right2'], #{, }, inverse <, >
        ['%', 'lr'], #% en spiegelbeeld
        ]
    fMatch = [
        ['bol', 'down', 'right2'],
        ['{', 'down', 'right2'],
        ]

    def __len__(self):
        """Tel het aantal figuren dat kan worden gemaakt.
        >>> len(FigurenSet('mini'))
        14
        """
        total = 0
        for beschrijving in self.verzameling:
            total += 1<<len(beschrijving)-1
        return total

    def kiesFiguur(self):
        """Maak een willekeurige figuur gegeven de lijst van beschrijvingen van borden.
        Deze mogelijkheden bestaat uit een lijst van lijstjes;
        ieder lijstje is [figuur, variant, variant, ...]
        """
        #verzin een combinatie
        nummer = randrange(len(self))
        #zoek de relevante groep combinaties
        for beschrijving in self.verzameling:
            aantalOpties = 1<<len(beschrijving)-1
            if nummer<aantalOpties: break
            nummer -= aantalOpties
        #zoek de bijpassende variatie
        figuur = Figuur(beschrijving[0])
        for index in range(1, len(beschrijving)):
            if not nummer>>index-1&1: continue
            #roep een optie aan. Kijk naar argument
            optie = beschrijving[index]
            if optie[-1].isdigit():
                figuur = getattr(figuur, optie[:-1])(int(optie[-1]))
            else:
                figuur = getattr(figuur, optie)()
        return figuur

    def maakAlleFiguren(self):
        """Genereer alle mogelijke figuren die je van een verzameling
        kan maken.
        >>> print(len(FigurenSet('klein').maakAlleFiguren()))
        13
        >>> print(len(FigurenSet('klein')))
        13
        """
        alleMogelijkheden = []
        #ga de groepen combinaties af
        for beschrijving in self.verzameling:
            #stop basisfiguur in z'n eentje in een lijst
            figuren = [Figuur(beschrijving[0])]
            #genereer variaties
            for optie in beschrijving[1:]:
                meerFiguren = []
                for figuur in figuren:
                    #roep een optie aan. Kijk naar argument
                    if optie[-1].isdigit():
                        nieuweFiguur = getattr(figuur, optie[:-1])(int(optie[-1]))
                    else:
                        nieuweFiguur = getattr(figuur, optie)()
                    meerFiguren.append(nieuweFiguur)
                #combineer met bestaande figuren
                figuren.extend(meerFiguren)
            #voeg de combinaties voor deze beschrijving toe
            alleMogelijkheden.extend(figuren)
        return alleMogelijkheden

    def perZijkant(self, kant):
        """Groepeer de figuren per linker- of rechterzijkant.
        >>> FigurenSet('mini').perZijkant('links')['>']
        [\GGGGGG/
         \GGGG/ 
         /GGGG\ 
        /GGGGGG\, \GGGGG\ 
         \GGGGG\\
         /GGGGG/
        /GGGGG/ ]
        """
        #indexeer op linkerkantvorm
        try: kantIndex = {'links':0, 'rechts':1}[kant]
        except KeyError: raise ValueError("kant moet 'links' of 'rechts' zijn'")
        result = {}
        for figuur in self.maakAlleFiguren():
            result.setdefault(
                figuur.zijkanten()[kantIndex], []).append(figuur)
        return result

class Bord:
    '''Representeer een bord.
    Een bord kan 12 MiniLoco of 24 Loco stukjes hebben.
    Met een mooie functie om een loco bord van chique kleurovergang
    te voorzien!
    '''
    def __init__(self, data = [0]*24):
        '''Leeg bord.
        Geef 'mini' als bord voor een leeg mini bord,
        of een lijst van 12 of 24 integers.
        >>> b = Bord()
        >>> b.mini
        False
        >>> Bord(range(1,25))
        BB\  /GG /RR\   GG\ RR\ 
        BBB\/GGG/RRRB\  GGG\RRR\\
           /   /   /\    /BB\   
          /G  /B  /RG\  /BBBR\  
          \B  \G\RRRR/  G/  B/  
           \   \ \RR/   /   /   
        \GGG\BBBRRR/GGG/  \RBBB/
         \GG \BBRR/ GG/    \BB/ 
        >>> Bord(range(1,13))
        GG\  /BB\RRR /GG /RRBB\ 
        GGG\/BBB \RR/GGG/RRRBBB\\
        \GGG\BBBRRR/GGG/RR\ BBB/
         \GG \BBRR/ GG/ RRR\BB/ 
        >>> b = Bord('mini')
        >>> b.mini
        True

        Verder bevat deze klasse een aardige hoeveelheid
        beschrijving om borden te maken.
        >>> for name in dir(FigurenSet):
        ...    if name.startswith('f'): print(name)
        fGroot
        fKlein
        fLijnsymm
        fMatch
        fMiddel1
        fMiddel2
        fMini
        fPuntsymm
        fShift
        '''
        if data=='mini': data = [0]*12
        if len(data)==12: self.mini = True
        elif len(data)==24: self.mini = False
        else: raise ValueError('Er gaan 12 of 24 blokjes in een Bord')
        self.bord = list(map(self.mini and MiniBlokje or Blokje, data))

    def __repr__(self):
        '''Geef een bord weer.
        '''
        #ondersteboven, zoals je de doos omdraait
        result = []
        for firstOfLine in range(self.mini and 12-5 or 24-5, 0, -6):
            regels = [blokje.achterkant() for blokje in self.bord[firstOfLine-1:firstOfLine+5]]
            for index in 0,1:
                result.append(''.join(regel[index] for regel in regels))
        return '\n'.join(result)

    def _str__(self):
        return 'Bord([%s])'%(','.join(
            str(b.num) for b in self.bord))

    def __setitem__(self, plaats, blokje):
        '''
        >>> b = Bord()
        >>> b[0] = Blokje(3)
        Traceback (most recent call last):
            [...]
        ValueError: Plaats klopt niet: 0
        >>> b[1] = Blokje(3)
        >>> b[2] = 3
        Traceback (most recent call last):
            [...]
        ValueError: Er moet een blokje in
        >>> b[3] = MiniBlokje(3)
        Traceback (most recent call last):
            [...]
        ValueError: Blokje in miniloco, of andersom
        >>> b
        <BLANKLINE>
        <BLANKLINE>
        <BLANKLINE>
        <BLANKLINE>
        <BLANKLINE>
        <BLANKLINE>
        RRR/                    
        RR/                     
        >>> b = Bord('mini')
        >>> b[1] = Blokje(3)
        Traceback (most recent call last):
            [...]
        ValueError: Blokje in miniloco, of andersom
        >>> b[0] = MiniBlokje(3)
        Traceback (most recent call last):
            [...]
        ValueError: Plaats klopt niet: 0
        '''
        if not isinstance(plaats, int): raise TypeError('Plaats moet een int zijn')
        if not isinstance(blokje, (Blokje, MiniBlokje)):
            raise ValueError('Er moet een blokje in')
        if isinstance(blokje, MiniBlokje)!=self.mini:
            raise ValueError('Blokje in miniloco, of andersom')
        if not 0<plaats<=(self.mini and 12 or 120):
            raise ValueError('Plaats klopt niet: %d'%plaats)
        self.bord[plaats%24-1] = blokje

    def maakOvergang(self):
        '''Geavanceerde kleurovergang voor borden die uit figuren bestaan
        waarvan een figuur net een kwart blokje doorloopt in de figuur ernaast.
        Hij gaat uit van drie naast elkaar geplaatste figuren.

        Hij zoekt naar groepjes van blokjes in een kolom
        die aan bovenkant en onderkant zijn afgesloten
        en die een "kleurprobleem" hebben.
        De afsluiting boven/onder kan zijn door de rand, of door wit.
        Anders geformuleerd, alle blokjes hebben boven of onder zich:
        - een rand
        - een ander blokje van de groep
        - een blokje zonder kleuraansluiting
        En verder moet het ook voldoen aan:
        - aan de kant van dezelfde kolom: wit
        - aan de kant van de andere kolom: kleur
        - aan de andere kant van de andere kolom: ook kleur
        Dit patroon moet zich dan mooi herhalen met de andere kolommen,
        anders komen de kleuren niet uit, natuurlijk.

        Een paar voorbeelden uit de praktijk:
        GGG/    \            G/
        GG/     G\           /
        G/      G/     \     \
        /       /      G\    G\
                       G/
                       /
        Verder kunnen we gemakkelijk inzien dat:
        - de blokjes dezelfde kleur hebben
        - de aansluiting ook dezelfde kleur heeft, maar een andere dan de blokjes
        Types zijn deelverzamelingen van [0,1,2,3]
        Als er drie zijn van een type
        worden ze geroteerd om de kleuren in orde te maken.
        Als er zes zijn, en ze passen in paren aan elkaar,
        worden de kleuren "omgeklapt".

        >>> b = zetNaastElkaar((
        ... Figuur('>>').right(),
        ... Figuur('S').right(),
        ... Figuur('..').right()))
        >>> b
          \RRR\    /GGG/   /BB\ 
           \RRR\  /GGG/   /BBBB\\
           /RRR/  \GGG\   \BBBB/
          /RRR/    \GGG\   \BB/ 
        \   \RRR\   \GGG\    /BB
        R\   \RRG\   \GGB\  /BBB
        R/   /RRG/   /GGB/  \BBB
        /   /RRR/   /GGG/    \BB
        >>> b.maakOvergang()
        >>> b
          \RRR\    /GGG/   /BB\ 
           \RRR\  /GGG/   /BBBB\\
           /RRR/  \GGG\   \BBBB/
          /RRR/    \GGG\   \BB/ 
        \   \RRR\   \GGG\    /BB
        B\   \RRR\   \GGG\  /BBB
        B/   /RRR/   /GGG/  \BBB
        /   /RRR/   /GGG/    \BB
        >>> b = zetNaastElkaar(3*[Figuur('miniruit').right(2)])
        >>> b.maakOvergang()
        >>> b
        \RRRRRR/\GGGGGG/\BBBBBB/
         \RRRR/  \GGGG/  \BBBB/ 
        \      /\      /\      /
        G\    /BB\    /RR\    /G
        G/    \BB/    \RR/    \G
        /      \/      \/      \\
         /RRRR\  /GGGG\  /BBBB\ 
        /RRRRRR\/GGGGGG\/BBBBBB\\
        >>> b = zetNaastElkaar(3*[Figuur('peer').inverse()])
        >>> b
        RRR/ /RRGGG/ /GGBBB/ /BB
        RR/ /RRRGG/ /GGGBB/ /BBB
        R/  \RRRG/  \GGGB/  \BBB
        /    \RR/    \GG/    \BB
        \     \R\     \G\     \B
        R\     \G\     \B\     \\
        RR\    /GG\    /BB\    /
        RRR\  /RGGG\  /GBBB\  /B
        >>> b.maakOvergang() #peer.inverse gaat helemaal mis!
        >>> b
        GGG/ /BBBBB/ /RRRRR/ /GG
        GG/ /BBBBB/ /RRRRR/ /GGG
        G/  \BBBB/  \RRRR/  \GGG
        /    \BB/    \RR/    \GG
        \     \B\     \R\     \G
        G\     \B\     \R\     \\
        GG\    /BB\    /RR\    /
        GGG\  /BBBB\  /RRRR\  /G
        >>> #Controleer of er niks gebeurt bij mini
        >>> Bord('mini').maakOvergang()
        >>> b = zetNaastElkaar(3*[Figuur('V').inverse()])
        >>> b.maakOvergang()
        >>> b
        \RRRRRR/\GGGGGG/\BBBBBB/
         \RRRR/  \GGGG/  \BBBB/ 
          \RR/    \GG/    \BB/  
           \/      \/      \/   
        \      /\      /\      /
        G\    /BB\    /RR\    /G
        GG\  /BBBB\  /RRRR\  /GG
        GGG\/BBBBBB\/RRRRRR\/GGG
        '''
        #controleer de voorwaarden
        #minibord doen niet mee bij dit moeilijke werk
        if self.mini: return
        #controleer welke blokjes per kolom aan de voorwaarden voldoen
        #voor details zie boven
        #hou per gevonden patroon de kolommen bij waarin dit voorkomt
        kolommenPerPatroon = {}
        for kolom in range(6):
            patroon = []
            naastKolom = kolom&1 and kolom+1 or kolom-1
            for rij6 in range(0, 24, 6):
                blokje = self.bord[kolom+rij6]
                #kleur moet aan de kant van de kolom zitten
                #(kleur zit maar aan een kant, dus wit in zelfde kolom)
                if blokje.linkerkantKleur()==kolom&1:
                    #kleur zit aan de verkeerde kant
                    continue
                #controleer andere kolom
                #buitenkant is altijd goed
                if 0<=naastKolom<6:
                    naastBlokje = self.bord[naastKolom+rij6]
                    if naastBlokje.linkerkantKleur()==naastKolom&1:
                        #kleur zit aan de verkeerde kant
                        continue
                #boven en onder doen we buiten de lus
                #blokje is goed to zo ver
                patroon.append(rij6)
            #controleer of de blokjes boven/onder zijn afgesloten
            for rij6 in patroon:
                blokje = self.bord[kolom+rij6]
                #bovenste rij is automatisch in orde
                rijBoven = rij6+6
                if rijBoven<24 and rijBoven not in patroon:
                    #controleer bovenkant (bovenste rij is 18!)
                    #rij is niet bovenaan, en blokje erboven zit niet in patroon
                    #check kleurenovergang naar boven
                    bovenBlokje = self.bord[kolom+rijBoven]
                    if blokje.bovenkantKleur() & bovenBlokje.onderkantKleur():
                        #er is kleurcontact: dit patroon werkt niet
                        break
                #doe onderste rij op precies dezelfde manier
                rijOnder = rij6-6
                if rijOnder>=0 and rijOnder not in patroon:
                    onderBlokje = self.bord[kolom+rijOnder]
                    if blokje.onderkantKleur() & onderBlokje.bovenkantKleur():
                        break
            else:
                #blokjes zijn gecontroleerd; voeg patroon toe
                kolommenPerPatroon.setdefault(tuple(patroon), []).append(kolom)

        #we hebben nu in kolommenPerPatroon het volgende
        #een patroon geeft aan welke rijen graag verwisseld willen worden
        #de bijbehorende kolommen geven aan op welke plekken dit moet
        #als de kolommen 0, 2, 4 zijn, dan moeten de blokjes met links matchen
        #dus roteren we ze naar rechts (evenOffset=2)
        #1,3,5 is ongeveer hetzelfde (oddOffset=-2)
        #als het alle kolommen zijn, nemen we de derde kleur voor de blokjes
        #dus evenOffset=-2 en oddOffset=2
        #strijdige offsets leiden tot stoppen met het hele feest

        #kladversie van het bord
        bord = self.bord[:]
        for patroon, kolommen in list(kolommenPerPatroon.items()):
            evenOffset = oddOffset = 0
            if kolommen==[0,2,4] or kolommen==[0,2,4,5]:
                evenOffset = 2
            elif kolommen==[1,3,5] or kolommen==[0,1,3,5]:
                oddOffset = -2
            elif kolommen==[0,1,2,3,4,5]:
                evenOffset = -2
                oddOffset = 2
            else: continue
            #OK, schuiven maar
            for kolom in range(6):
                if kolom&1: nieuweKolom = kolom+oddOffset
                else: nieuweKolom = kolom+evenOffset
                nieuweKolom %= 6
                for rij6 in patroon:
                    bord[nieuweKolom+rij6] = self.bord[kolom+rij6]
                    #kijk of het wel klopt
                    hoek, pos, vol, kleur = self.bord[nieuweKolom+rij6].eigenschappen()
                    ohoek, opos, ovol, okleur = self.bord[kolom+rij6].eigenschappen()
                    if ohoek!=hoek or opos!=pos or ovol!=vol:
                        #oeps, het gaat mis
                        return
        #en nu is het klaar
        self.bord = bord
        return

    @staticmethod
    def verzinBord(keuze = "random"):
        '''Maak een random bord.
        Regelmatigheid is instelbaar:
        - keuze voor gewone loco
            symm: hele bord punt- of lijnsymmetrisch
            klein: drie verschillende van *, ruit, <, >, v, ^, zandloper
            (de rest is drie dezelfde)
            middel: alle genoemd in Figuur, met varianten
            groot: ook peer, miniruit en dubbelruit
            shift: >, <<, <>, .., <., met "kleine" rotaties erbij
            alles: drie dezelfde groot of shift.
        - keuze voor miniloco
            mini: een figuur voor miniloco
            match: drie verschillende die op elkaar passen voor miniloco
            minisym: symmetrische miniloco figuur (in aanbouw)
        - als je niet zelf wil kiezen, kies je "random" of "minirandom"
        >>> seed(5)
        >>> Bord.verzinBord('shift')
        \    /BB\    /RR\    /GG
        G\  /BBBB\  /RRRR\  /GGG
        G/  \BBBB/  \RRRR/  \GGG
        /    \BB/    \RR/    \GG
        GG\    /BB\    /RR\    /
        GGG\  /BBBB\  /RRRR\  /G
        GGG/  \BBBB/  \RRRR/  \G
        GG/    \BB/    \RR/    \\
        >>> Bord.verzinBord('mini')
        RRR/RR\ GGG/GG\ BBB/BB\ 
        RR/ RRR\GG/ GGG\BB/ BBB\\
        \RRR /RR\GGG /GG\BBB /BB
         \RR/RRR \GG/GGG \BB/BBB
        >>> Bord.verzinBord('minisym')
        RR\ \RRRBBB/\BBBGGG/ /GG
        RRR\ \RRBB/  \BBGG/ /GGG
        RRR/ /RRBB\  /BBGG\ \GGG
        RR/ /RRRBBB\/BBBGGG\ \GG
        '''
        #bepaal de verzameling waaruit we kiezen
        if keuze=="random": keuze = choice(['alles', 'klein', 'symm'])
        elif keuze=="minirandom": keuze = choice(['mini', 'match', 'minisym'])
        if keuze=='klein':
            #drie verschillende figuren
            figuren = Bord.verzinKlein()
        elif keuze=='match':
            #drie verschillende minifiguren
            figuren = Bord.verzinMatch()
        elif keuze=='symm':
            #drie verschillende figuren
            figuren = Bord.verzinSymm()
        elif keuze=='minisym':
            #drie verschillende figuren
            figuren = Bord.verzinMinisym()
        else:
            #drie dezelfde figuren
            if keuze=='middel':
                verzameling = ('middel1','middel2')
            elif keuze=='groot':
                verzameling = ('middel1','middel2','groot')
            elif keuze=='shift':
                verzameling = 'shift',
            elif keuze=='alles':
                verzameling = ('middel1','groot','shift')
            elif keuze=='mini':
                verzameling = 'mini',
            else: raise ValueError("verzinBord: invalid argument")
            figuren = 3*[FigurenSet(*verzameling).kiesFiguur()]
        #vul kleuren in
        rgb = list('RGB')
        shuffle(rgb)
        result = zetNaastElkaar(figuren, rgb)
        #maak plaatje mooier als mogelijk
        result.maakOvergang()
        return result

    @staticmethod
    def verzinMatch():
        """Verzin een bord met de figuren uit de kleine verzameling,
        drie verschillende naast elkaar.
        Inverteert de hele constructie af en toe.
        >>> seed(5)
        >>> Bord.verzinBord('match')
         /RRRRR/ /GGGGG/ /BBBBB/
        /RRRRR/ /GGGGG/ /BBBBB/ 
        \RRRRR\ \GGGGG\ \BBBBB\ 
         \RRRRR\ \GGGGG\ \BBBBB\\
        >>> Bord.verzinBord('match')
        RR\ \RRRGG\  /GGBBB/\BBB
        RRR\ \RRGGG\/GGGBB/  \BB
        RRR/ /RRGGG/\GGGBB\  /BB
        RR/ /RRRGG/  \GGBBB\/BBB
        >>> Bord.verzinBord('match')
        BB\ \BBBGG\  /GGRRR/\RRR
        BBB\ \BBGGG\/GGGRR/  \RR
        BBB/ /BBGGG/\GGGRR\  /RR
        BB/ /BBBGG/  \GGRRR\/RRR
        >>> Bord.verzinBord('match')
         /BBBBB/ /GGGG\ \RRRRRR/
        /BBBBB/ /GGGGGG\ \RRRR/ 
        \BBBBB\ \GGGGGG/ /RRRR\ 
         \BBBBB\ \GGGG/ /RRRRRR\\
        """
        alleMogelijkheden = FigurenSet('match')
        figurenPerLinks = alleMogelijkheden.perZijkant('links')

        #nu kunnen we aan de slag: maak drie figuren die mooi aansluiten
        #eerst maar eens eentje helemaal vrij gekozen
        figuren = [alleMogelijkheden.kiesFiguur()]
        for i in range(2):
            #plak er eentje naast...
            figuren.append(
                #willekeurig...
                choice(
                    #passend aan de linkerkant...
                    figurenPerLinks[
                        #op de meest rechts figuur...
                        figuren[-1]
                            #zijn rechterkant!
                            .zijkanten()[1]]))
        return figuren

    @staticmethod
    def verzinSymm():
        """Verzin een loco bord met lijn- of puntsymmetrie.
        >>> seed(11)
        >>> Bord.verzinBord('symm')
           /RRR/   /\   \GGG\   
          /RRR/   /BB\   \GGG\  
          \RRR\   \BB/   /GGG/  
           \RRR\   \/   /GGG/   
         /RR\    /BBBB\    /GG\ 
        /RRRR\  /BBBBBB\  /GGGG\\
        \RRRR/  \BBBBBB/  \GGGG/
         \RR/    \BBBB/    \GG/ 
        """
        #Kies lijn of punt
        lijnOfPunt = choice(('lijn','punt'))
        middelFiguur = FigurenSet(lijnOfPunt+'symm').kiesFiguur()
        linkerkant = middelFiguur.zijkanten()[0]

        kantfiguren = FigurenSet('middel1','groot','shift')
        #maak een lijst van relevante symbolen
        figurenPerRechterkant = kantfiguren.perZijkant('rechts')
        #Kies een eerste symbool: maak eerste 
        linkerFiguur = choice(figurenPerRechterkant[linkerkant])
        if lijnOfPunt=='lijn':
            rechterFiguur = linkerFiguur.lr()
        else:
            rechterFiguur = linkerFiguur.lr().ud()
        figuren = [linkerFiguur, middelFiguur, rechterFiguur]
        if randrange(2):
            figuren = [f.inverse() for f in figuren]
        return figuren

    @staticmethod
    def verzinMinisym():
        """Verzin een miniloco bord met lijn- of puntsymmetrie.
        >>> seed(10)
        >>> Bord.verzinBord('minisym')
        BBB/ /BBGG\  /GGRR\ \RRR
        BB/ /BBBGGG\/GGGRRR\ \RR
        BB\ \BBBGGG/\GGGRRR/ /RR
        BBB\ \BBGG/  \GGRR/ /RRR
        """
        #Kies lijn of punt
        lijnOfPunt = choice(('Lijn','Punt'))
        #verzamel relevante figuren
        figuren = FigurenSet('mini').maakAlleFiguren()
        #zoek figuren met relevante symmetrie
        LijnSymmetrischeFiguren = []
        PuntSymmetrischeFiguren = []
        for figuur in figuren:
            if figuur.lr()==figuur: LijnSymmetrischeFiguren.append(figuur)
            if figuur.lr().ud()==figuur: PuntSymmetrischeFiguren.append(figuur)
        #begin in het midden
        middelFiguur = choice(locals()[lijnOfPunt+'SymmetrischeFiguren'])
        rechterZijkant = middelFiguur.zijkanten()[0]
        #kies linkersymbool: maak eerst categorieen
        figurenPerRechterkant = {}
        for figuur in figuren:
            figurenPerRechterkant.setdefault(figuur.zijkanten()[1],[]).append(figuur)
        mogelijkVoorLinks = figurenPerRechterkant[rechterZijkant]
        if rechterZijkant in '<>':
            zijkantSpiegelbeeld = {'<':'>', '>':'<'}[rechterZijkant]
            mogelijkVoorLinks.extend(figurenPerRechterkant[zijkantSpiegelbeeld])
        linkerFiguur = choice(mogelijkVoorLinks)
        if lijnOfPunt=='Lijn':
            rechterFiguur = linkerFiguur.lr()
        else:
            rechterFiguur = linkerFiguur.lr().ud()
        figuren = [linkerFiguur, middelFiguur, rechterFiguur]
        return figuren

    @staticmethod
    def verzinKlein():
        """Verzin een miniloco bord met drie verschillende figuren die
        aan elkaar passen.
        >>> seed(4)
        >>> Bord.verzinBord('klein')
        \GGG\      /\      /R/  
         \GGG\    /BB\    /R/   
          \GGG\  /BBBB\  /RR\   
           \GGG\/BBBBBB\/RRRR\  
           /GGG/\BBBBBB/\RRRRR\ 
          /GGG/  \BBBB/  \RRRRR\\
         /GGG/    \BB/    \RRRR/
        /GGG/      \/      \RR/ 
        """
        #Voor miniklein, we willen zijkanten matchen
        kleineFiguren = FigurenSet('klein')
        figuren = [
            kleineFiguren.kiesFiguur()
            for i in range(3)
            ]
        if randrange(2):
            figuren = [f.inverse() for f in figuren]
        return figuren

        
class Figuur:
    '''Representeer een figuur die een goede oplossing voorstelt.
    Staat operaties toe om varianten te maken,
    en kan een bord uitvoeren met de figuur erop.
    Deze figuren zijn eigenlijk overbodig:
    >>> Figuur('zandloper') == Figuur('ruit').down(2)
    True
    >>> Figuur('<') == Figuur('>').lr() == Figuur('>').down(2)
    True
    >>> Figuur('S') == Figuur('>').down()
    True
    >>> Figuur('^') == Figuur('V').ud()
    True
    >>> Figuur('miniruit') == Figuur('dubbelruit').swap().down()
    True
    >>> Figuur('kaars') == Figuur('ruit').down().swap(1)
    True
    >>> Figuur('><').swap() == Figuur('>>')
    True
    >>> Figuur('bol').down() == Figuur('gordijn')
    True
    >>> Figuur('%')
     /GG\GGG
    /GGG \GG
    GG\ GGG/
    GGG\GG/ 
    '''
    #eenvoudige standaardfiguren
    #je kan er meer maken met de operaties
    voorbeelden = {
        '*': r'g\/g/GG\\GG/g/\g',
        'ruit': r'g\/g\GG//GG\g/\g',
        'zandloper': r'/GG\g/\gg\/g\GG/',
        '>': r'/G/gg/G/g\G\\G\g',
        '<': r'g\G\\G\g/G/gg/G/',
        'S': r'g/G/g\G\\G\g/G/g',
        'V': r'g\/g\GG/G\/G\gg/',
        '^': r'/gg\G/\G/GG\g/\g',
        'peer': r'g\G/\GG\/G\gg//g',
        'dubbelruit': r'G\/GG/\Gg\/gg/\g',
        'miniruit': r'G\/Gg\/gg/\gG/\G',
        'kaars': r'\GG//GG\g\/gg/\g',
        '>>': r'g/G/g\G\/G/g\G\g',
        '>.': r'g\G/g/G\/G/g\G\g',
        '..': r'g\G/g/G\\G/g/G\g',
        '><': r'g\G\g/G//G/g\G\g',
        #figuren voor miniloco
        'bol': r'\GG//GG\\'[:-1],
        'v': r'\GG/G\/G',
        '{': r'\GG\/GG/',
        'gordijn': r'/GG\\GG/',
        '%': r'G\G//G\G',
        }
    def checkset(voorbeelden):
        return set([voorbeelden['*'][i:i+2] for i in range(0,16,2)])
    checkset = checkset(voorbeelden)
    def minicheckset(voorbeelden):
        return set([voorbeelden['bol'][i:i+2] for i in range(0,8,2)])
    minicheckset = minicheckset(voorbeelden)
    swapslash = list(map(chr, list(range(256))))
    swapslash[ord('/')] = '\\'
    swapslash[ord('\\')] = '/'
    swapslash = ''.join(swapslash)

    def __init__(self, naam='ruit'):
        '''Maak een standaardfiguur, of van een code.
        Met operaties om de figuur aan te passen.
        >>> Figuur().mini
        False
        >>> Figuur('bol').mini
        True
        >>> Figuur('peer')
           /G/  
          /G/   
         /GG\   
        /GGGG\  
        \GGGGG\ 
         \GGGGG\\
          \GGGG/
           \GG/ 
        >>> seed(5)
        >>> Figuur('troep')
         /GG   /
        /GGG  /G
        \   GGG/
        G\  GG/ 
        GG\ G/  
        GGG\/   
        \GGG  \G
         \GG   \\
        >>> Figuur('bol').code #sorry voor de vierdubbele backslashes
        '\\\\GG//GG\\\\'
        '''
        if naam in Figuur.voorbeelden:
            self.code = Figuur.voorbeelden[naam]
        elif set(naam[i:i+2] for i in range(0,16,2)) == Figuur.checkset:
            self.code = naam
        elif set(naam[i:i+2] for i in range(0,8,2)) == Figuur.minicheckset:
            self.code = naam
        elif naam == 'troep':
            code = list(Figuur.checkset)
            shuffle(code)
            self.code = ''.join(code)
        else:
            raise ValueError('Kan geen Figuur maken: '+naam)
        if len(self.code)==8: self.mini = True
        elif len(self.code)==16: self.mini = False
        else: raise ValueError('Lengte van Figuur klopt niet: '+repr(len(self.code)))

    def blokjes(self):
        """Geef de blokjes waaruit de figuur bestaat.
        >>> Figuur('bol').blokjes()
        [MiniBlokje(1), MiniBlokje(4), MiniBlokje(10), MiniBlokje(7)]
        """
        maakBlokje = self.mini and MiniBlokje or Blokje
        return [maakBlokje(self.code[i:i+2]) for i in range(0, len(self.code), 2)]

    def lr(self):
        '''Spiegel links-rechts.
        >>> Figuur('peer').lr()
          \G\   
           \G\  
           /GG\ 
          /GGGG\\
         /GGGGG/
        /GGGGG/ 
        \GGGG/  
         \GG/   
        '''
        c = self.code
        result = ''
        for i in range(0,self.mini and 8 or 16,4):
            #irritant: python gaat bij -1 van de andere kant
            result += c[i+3:i>0 and i-1 or None:-1]
        return Figuur(result.translate(Figuur.swapslash))

    def ud(self):
        '''Spiegel op-neer.
        >>> Figuur('peer').ud()
           /GG\ 
          /GGGG\\
         /GGGGG/
        /GGGGG/ 
        \GGGG/  
         \GG/   
          \G\   
           \G\  
        '''
        c = self.code
        result = ''
        for i in range(self.mini and 8-4 or 16-4, -4, -4):
            result += c[i:i+4]
        return Figuur(result.translate(Figuur.swapslash))

    def inverse(self):
        '''Inverteer.
        >>> Figuur('peer').inverse()
        GGG/ /GG
        GG/ /GGG
        G/  \GGG
        /    \GG
        \     \G
        G\     \\
        GG\    /
        GGG\  /G
        >>> Figuur('{').inverse()
        Traceback (most recent call last):
            [...]
        AssertionError: Kan alleen inverse doen van grote loco
        '''
        assert not self.mini, 'Kan alleen inverse doen van grote loco'
        return Figuur(self.code.swapcase())

    def down(self, amount = 1):
        '''Roteer naar beneden van de figuur (blokjes gaan omhoog!).
        >>> Figuur('peer').down(1)
          \GGGG/
           \GG/ 
           /G/  
          /G/   
         /GG\   
        /GGGG\  
        \GGGGG\ 
         \GGGGG\\
        >>> Figuur('bol').down()
        \GGGGGG/
         \GGGG/ 
         /GGGG\ 
        /GGGGGG\\
        '''
        assert 0<amount<(self.mini and 2 or 4), 'down: kan niet %d roteren'%amount
        amount *= 4
        return Figuur(self.code[amount:]+self.code[:amount])

    def right(self, amount = 1):
        '''Roteer naar rechts over een half (!) blokje.
        Dit kan lang niet altijd!
        >>> Figuur().right()
        Traceback (most recent call last):
            [...]
        ValueError: schuif onmogelijk
        >>> Figuur('>').right()
          \GGG\ 
           \GGG\\
        \   \GGG
        G\   \GG
        G/   /GG
        /   /GGG
           /GGG/
          /GGG/ 
        '''
        if self.mini:
            assert amount==2, 'Kan alleen right 2 doen van miniloco'
        else:
            assert 0<amount<4, 'right: kan niet %d roteren'%amount
        amount = 4-amount
        c = self.code
        result = ''
        for i in range(0,self.mini and 8 or 16,4):
            result += c[i+amount:i+4]+c[i:i+amount]
        #print result
        #controleer of het klopt
        if set(result[i:i+2] for i in range(0,self.mini and 8 or 16,2)) != \
            (self.mini and Figuur.minicheckset or Figuur.checkset):
                raise ValueError('schuif onmogelijk')
        return Figuur(result)

    def switch(self):
        '''Roteer bovenste helft naar rechts, onderste naar links.
        Speciaal voor <<, <., en zo.
        >>> Figuur('>.').switch()
          \GGG\ 
           \GGG\\
           /GGG/
          /GGG/ 
         /GG\   
        /GGGG\  
        \GGGG/  
         \GG/   
        '''
        return Figuur(
            #onderste helft
            self.right(3).code[:8]+
            #bovenste helft
            self.right(1).code[8:])

    def swap(self, line=3):
        '''Verwissel regels n en n+1 met elkaar.
        >>> Figuur('..').swap(1)
        \GGGG/  
         \GG/   
         /GG\   
        /GGGG\  
           /GG\ 
          /GGGG\\
          \GGGG/
           \GG/ 
        >>> Figuur('kaars').swap()
           /\   
          /GG\  
          \GG/  
           \/   
        \GGGGGG/
         \GGGG/ 
         /GGGG\ 
        /GGGGGG\\
        '''
        assert not self.mini, 'Kan alleen swap doen van grote loco'
        assert 0<line<=4, 'swap: regel %d kan niet'%line
        #lines bevat de regels van onder naar boven: 4, 3, 2, 1
        lines = [self.code[i:i+4] for i in range(0,16,4)]
        lines[4-line], lines[3-line] = lines[3-line], lines[4-line]
        return Figuur(''.join(lines))

    def zijkanten(self):
        '''Geef aan hoe de zijkanten van een figuur eruitzien.
        Dit kan zijn:
        voor miniloco: <, >, |, h, l.
            <>| staan voor een grof plaatje van de zijkant;
            lh betekent kleur hoog of laag.
        voor gewone loco: h, l, b, 0 of ?. (hoog, laag, beide, niks:
            dit slaat op groepjes van twee blokjes:
            we kijken alleen naar kleur op de rand)
        >>> Figuur('bol').zijkanten()
        '<>'
        >>> Figuur('bol').right(2).zijkanten()
        '||'
        >>> Figuur('v').zijkanten()
        'hh'
        >>> Figuur('{').zijkanten()
        '<<'
        >>> Figuur('gordijn').zijkanten()
        '><'
        >>> Figuur('>').zijkanten()
        '00'
        >>> Figuur('miniruit').zijkanten()
        '??'
        >>> Figuur('dubbelruit').zijkanten()
        'll'
        '''
        if self.mini:
            #Dit zou logischer zijn met blokjes() en eigenschappen,
            #maar wordt niet veel leesbaarder. Dus we doen het ad hoc
            #code voor de zijkanten: maak van de slashes een symbool
            zijkantDict = {
                '/\\': '<',
                '\\/': '>',
                'GG': '|',
                'G/': 'h', 'G\\': 'h',
                '/G': 'l', '\\G': 'l',
                }
            #linkboven, en linksonder zijn blokjes 2 en 0,
            #dus hun eerste symbolen zijn 4 en 0
            links = zijkantDict[self.code[4]+self.code[0]]
            rechts = zijkantDict[self.code[7]+self.code[3]]
            return links+rechts
        else:
            #niet snel, wel duidelijk
            #boolean map: kleur zit aan de linkerkant
            linksMap = [b.linkerkantKleur() for b in self.blokjes()]
            #| staat voor kleur aan deze kant
            zijkantDict = {
                '||  ': 'h',
                '  ||': 'l',
                '||||': 'b',
                '    ': '0'}
            #nummering in leesvolgorde van linksboven naar rechtonder: 67452301
            links = zijkantDict.get(
                ''.join(l and '|' or ' ' for l in linksMap[6::-2]),
                '?')
            rechts = zijkantDict.get(
                ''.join(l and ' ' or '|' for l in linksMap[7::-2]),
                '?')
            return links+rechts

    def __repr__(self):
        '''De representatie.
        >>> Figuur('<')
           /GGG/
          /GGG/ 
         /GGG/  
        /GGG/   
        \GGG\   
         \GGG\  
          \GGG\ 
           \GGG\\
        >>> Figuur('{')
         /GGGGG/
        /GGGGG/ 
        \GGGGG\ 
         \GGGGG\\
        '''
        if self.mini:
            regels = [MiniBlokje(self.code[i:i+2]).achterkant()
                for i in range(0,8,2)]
        else:
            regels = [Blokje(self.code[i:i+2]).achterkant()
                for i in range(0,16,2)]
        result = []
        for firstOfLine in range(self.mini and 2 or 6, -2, -2):
            for index in 0, 1:
                result.append(regels[firstOfLine][index]
                    +regels[firstOfLine+1][index])
        return '\n'.join(result)

    def __eq__(self, other): return self.code==other.code
    def __ne__(self, other): return self.code!=other.code
    
    def __hash__(self):
        """Hash fucntion, handig voor set()
        >>> for name in dir(FigurenSet):
        ...    if name.startswith('f'):
        ...        verzameling = FigurenSet(name[1:]).maakAlleFiguren()
        ...        print(name, len(verzameling), len(set(verzameling)))
        fGroot 24 24
        fKlein 13 13
        fLijnsymm 23 23
        fMatch 8 8
        fMiddel1 12 12
        fMiddel2 32 32
        fMini 14 14
        fPuntsymm 16 16
        fShift 72 72
        """
        return hash(self.code)

def zetNaastElkaar(figuren, kleuren='RGB'):
    '''Zet drie figuren naast elkaar.
    >>> zetNaastElkaar((Figuur('*'), Figuur('ruit'), Figuur('S')))
       /\      /\    /BBB/  
      /RR\    /GG\  /BBB/   
    \RRRRRR/ /GGGG\ \BBB\   
     \RRRR/ /GGGGGG\ \BBB\  
     /RRRR\ \GGGGGG/  \BBB\ 
    /RRRRRR\ \GGGG/    \BBB\\
      \RR/    \GG/     /BBB/
       \/      \/     /BBB/ 
    >>> zetNaastElkaar((Figuur('bol'), Figuur('v'), Figuur('{')))
     /RRRR\ GG\  /GG /BBBBB/
    /RRRRRR\GGG\/GGG/BBBBB/ 
    \RRRRRR/\GGGGGG/\BBBBB\ 
     \RRRR/  \GGGG/  \BBBBB\\
    >>> zetNaastElkaar((Figuur('*'),), 'GBR')
       /\      /\      /\   
      /GG\    /BB\    /RR\  
    \GGGGGG/\BBBBBB/\RRRRRR/
     \GGGG/  \BBBB/  \RRRR/ 
     /GGGG\  /BBBB\  /RRRR\ 
    /GGGGGG\/BBBBBB\/RRRRRR\\
      \GG/    \BB/    \RR/  
       \/      \/      \/   
    '''
    #zoek het type uit
    mini = figuren[0].mini
    result = Bord([0]*(mini and 12 or 24))
    if len(figuren)==1: figuren *= 3
    if len(kleuren)!=3: raise ValueError('3 kleuren graag!')
    if len(figuren)!=3: raise ValueError('1 of 3 figuren graag!')
    for gedeelte, figuur, kleur in zip((0,1,2), figuren, kleuren):
        assert kleur in 'RGB', 'Kleur %s bestaat niet'%kleur
        for i in range(mini and 4 or 8):
            code = figuur.code[i*2:i*2+2]
            code = code.replace('G',kleur).replace('g',kleur.lower())
            #sorry voor de indexberekening :-)
            #3*(i&6): offset verticaal
            #(i&1): offset horizontaal
            #1: we beginnen te tellen bij 1
            #2*gedeelte: keuze van het blokje
            result[3*(i&6)+(i&1)+1+2*gedeelte] = (mini and MiniBlokje or Blokje)(code)
    return result

#-------------routines voor het maken van een opgavenformulier---------
def regelUitkomsten(opgaven):
    '''Maak een afbeelding van de opgaven naar de getallen 1 tot 24,
    rekening houdend met de 5 getallen op de stenen.
    De opgaven zijn paren (opgave, antwoord); antwoord moet
    tussen 0 en 121 liggen.
    Geef terug een lijst van de opgaven waar 1,...,24 uitkomt.
    >>> seed(17)
    >>> regelUitkomsten(vermenigvuldigingen())
    ['6*12+1=', '6*12+2=', '6*13-3=', '10*10=', '7*11=', '6*9=', '10*8-1=', '13*8=', '8*13+1=', '10*8+2=', '12*7-1=', '7*12=', '10*11-1=', '11*10=', '11*8-1=', '11*8=', '8*11+1=', '11*6=', '13*7=', '9*13-1=', '9*13=', '7*10=', '6*12-1=', '8*9=']
    '''
    #hulpfuncties

    def passendeOpgaven(gat):
        '''Vind min, max indices van opgaven
        die precies passen bij het gegeven gat%24.'''
        #er geldt:
        #opgaven[plekL]<=gat<gat+1<opgaven[plekR]
        #maar elke plek kan ook >=len(opgaven)-1 zijn
        plekL = bisect(opgaven, (gat, '', None))
        plekR = bisect(opgaven, (gat+1, '', None))
        #er zijn dus (plekR-plekL)%len(opgaven) exacte oplossingen
        return plekL, plekR

    def dichtbijeOpgaven(gat):
        '''Geef indices van alle dichtsbijzijnde opgaven tot gat.
        Dit is een niet-lege lijst, als er nog opgaven zijn.
        'Dichtbij is hier mod 24, dus je kan verkeerde opgaven terugkijken,
        zoals antwoord 23 die afstand 2 heeft tot gat 1.
        '''
        if len(opgaven)<3:
            pass
        exactL, exactR = passendeOpgaven(gat)
        waarden = list(range(exactL, exactR))
        if exactL<exactR:
            #we hebben een aantal exacte matches
            dist = 0
        else:
            #we hebben geen exacte match
            #zoek iets kleiner antwoord, bereken afstand (positieve correctie)
            waardeL = opgaven[(exactL-1)%len(opgaven)][0]
            afstandL = (gat-waardeL)%24
            #zoek iets groter antwoord, bereken afstand (negatieve correctie)
            waardeR, opgaveR, antwoordR = opgaven[exactR%len(opgaven)]
            afstandR = (waardeR-gat)%24
            #voeg een of twee minimale verzamelingen toe
            if afstandL<=afstandR:
                waarden.extend(list(range(*passendeOpgaven(waardeL))))
                dist = afstandL
            if afstandR<=afstandL:
                waarden.extend(list(range(*passendeOpgaven(waardeR))))
                dist = afstandR
        return dist, waarden
    #we gaan aan de slag
    #sorteer de opgaven
    opgaven = [(antwoord%24, opgave, antwoord)
            for opgave, antwoord in opgaven]
    opgaven.sort()
    result = [None]*24
    #hou bij welke uitkomsten ('gaten') we nog moeten
    vrijeGaten = list(range(24))
    #hou bij hoe dichtbij we willen komen in streefAfstand
    #wordt geinitialiseerd via deze variabele (komt straks)
    besteMislukteAfstand = 0
    while vrijeGaten:
        assert besteMislukteAfstand<99, 'Interne fout in regelUitkomsten'
        streefAfstand = besteMislukteAfstand
        #zoek willekeurige gaten bij huidige streefafstand
        dezeGaten = vrijeGaten[:]
        #van de gaten die niet lukken, houden we de beste afstand bij
        besteMislukteAfstand = 99
        while dezeGaten:
            #pak een gat om te proberen met deze afstand
            gat = choice(dezeGaten)
            afstand, keuzes = dichtbijeOpgaven(gat)
            if afstand==streefAfstand:
                #we kunnen de streefafstand halen; kies een opgave
                opgaveNummer = choice(keuzes)
                #bepaal correctie voor bij de opgave
                correctie = gat-opgaven[opgaveNummer][0]
                if correctie>12: correctie -= 24
                #controleer of opgave zo gaat werken
                if not 0<opgaven[opgaveNummer][2]+correctie<=120:
                    #oeps: we hebben bijvoorbeeld 1*2-4 voor gat 22
                    #deze opgave is te lastig; knikker maar weg
                    del opgaven[opgaveNummer]
                    #probeer de overgebleven opgaven
                    continue
                correctie = '%+d'%correctie
                if correctie=='+0': correctie = ''
                #Yes. sla antwoord op, haal opgave en gat van de lijst
                result[gat-1] = opgaven[opgaveNummer][1]+correctie+'='
                vrijeGaten.remove(gat)
                del opgaven[opgaveNummer]
            elif afstand<besteMislukteAfstand:
                #dit is mogelijk de nieuwe streefafstand
                besteMislukteAfstand = afstand
            #dit gat is gevuld met deze streefafstand
            dezeGaten.remove(gat)
    return result


def printFormulier(opgaven, uitleg='Opgaven (blokjes):',
        antwoorduitleg='Uitkomsten (vakjes):'):
    '''Druk een LOCO formulier af.
    Invoer is een lijst van opgaven op volgorde van uitkomst 1..12.
    Of een lijst van paren (opgave, uitkomst).
    Som is een string en uitkomst een getal.
    Als de uitkomsten strings zijn, komt er een lijst van antwoorden bij.
    Je mag ook tripels (opgave, uitkomst, waarde) geven:
    er wordt dan op waarde gesorteerd.
    >>> seed(12)
    >>> printFormulier(vermenigvuldigingen(4))
    Opgaven (blokjes):
     1: 6*8+1=               13: 7*13=
     2: 7*11=                14: 8*9-1=
     3: 7*8+2=               15: 7*9=
     4: 11*10=               16: 8*10=
     5: 8*12-3=              17: 7*12-1=
     6: 9*10=                18: 10*7=
     7: 11*6-1=              19: 6*12=
     8: 9*12+1=              20: 6*9+1=
     9: 9*11=                21: 9*9=
    10: 11*7-1=              22: 10*6=
    11: 12*6+2=              23: 6*11+2=
    12: 6*13=                24: 8*11=
    <BLANKLINE>
    Oplossing:
       /GG\   \R\      /BB\ 
      /GGGG\   \R\    /BBBB\\
      \GGGG/   /RR\   \BBBB/
       \GG/   /RRRR\   \BB/ 
     /GG\    /RRRRR/ /BB\   
    /GGGG\  /RRRRR/ /BBBB\  
    \GGGG/  \RRRR/  \BBBB/  
     \GG/    \RR/    \BB/   
    >>> printFormulier(miniOptellingen())
    Opgaven (blokjes):
     1: 4-1                   7: 7+2
     2: 5+0                   8: 11+1
     3: 5-3                   9: 3-2
     4: 10-6                 10: 8+2
     5: 7+0                  11: 9-1
     6: 3+3                  12: 3+8
    <BLANKLINE>
    Oplossing:
    RR\  /RRGG\  /GGBB\  /BB
    RRR\/RRRGGG\/GGGBBB\/BBB
    \RRRRRR/\GGGGGG/\BBBBBB/
     \RRRR/  \GGGG/  \BBBB/ 
    >>> printFormulier(getalMaalBreuk())
    Opgaven (blokjes):
     1: 2 x (3/5)            13: 7 x (2/3)
     2: 2 x (5/6)            14: 3 x (4/2)
     3: 5 x (3/7)            15: 7 x (4/4)
     4: 3 x (2/4)            16: 5 x (3/3)
     5: 8 x (3/6)            17: 1 x (3/5)
     6: 3 x (5/8)            18: 7 x (4/3)
     7: 5 x (4/7)            19: 2 x (2/5)
     8: 5 x (3/6)            20: 1 x (3/7)
     9: 2 x (4/4)            21: 1 x (6/7)
    10: 6 x (3/4)            22: 8 x (4/5)
    11: 7 x (3/8)            23: 1 x (2/4)
    12: 4 x (3/4)            24: 2 x (3/6)
    <BLANKLINE>
    Uitkomsten (vakjes):
     1:    3/7               13:  2 1/2
     2:    1/2               14:  2 5/8
     3:    3/5               15:  2 6/7
     4:    4/5               16:  3
     5:    6/7               17:  4
     6:  1                   18:  4 1/2
     7:  1 1/5               19:  4 2/3
     8:  1 1/2               20:  5
     9:  1 2/3               21:  6
    10:  1 7/8               22:  6 2/5
    11:  2                   23:  7
    12:  2 1/7               24:  9 1/3
    <BLANKLINE>
    Oplossing:
       /\      /\      /\   
      /GG\    /BB\    /RR\  
      \GG/    \BB/    \RR/  
       \/      \/      \/   
    \GGGGGG/\BBBBBB/\RRRRRR/
     \GGGG/  \BBBB/  \RRRR/ 
     /GGGG\  /BBBB\  /RRRR\ 
    /GGGGGG\/BBBBBB\/RRRRRR\\
    '''
    #voor miniloco moet je 12 opgaven in volgorde maken
    mini = len(opgaven)<24
    #verzin een bord
    if mini: bord = Bord.verzinBord("minirandom")
    else: bord = Bord.verzinBord("random")
    uitkomsten = None
    #zet opgaven op volgorde van de uitkomst als nodig
    if isinstance(opgaven[0], tuple) and isinstance(opgaven[0][1], str):
        #er zijn stringuitkomsten, maak een uitkomstenlijst
        #sorteer op uitkomst of sorteerwaarde
        opgaven.sort(key = itemgetter(-1))
        #pak uitkomsten en opgaven (voor tupels van twee en drie)
        uitkomsten = list(map(itemgetter(1), opgaven))
        opgaven = list(map(itemgetter(0), opgaven))
    elif not mini:
        #zet de opgaven op volgorde van uitkomst
        opgaven = regelUitkomsten(opgaven)
    #sorteer de opgaven op nummer van het blokje
    opgaven = dict(list(zip(
        list(map(attrgetter('num'), bord.bord)),
        opgaven)))
    print(uitleg)
    kolomBreedte = max(len(o) for o in list(opgaven.values()))+1
    kolomBreedte = max(20, kolomBreedte)
    for regel in range(1, mini and 7 or 13):
        volgendeRegel = regel+(mini and 6 or 12)
        print(('%2d: %-*s %2d: %s'%(
            regel,
            kolomBreedte,
            opgaven[regel],
            volgendeRegel,
            opgaven[volgendeRegel])))
    if uitkomsten:
        kolomBreedte = max(len(o) for o in uitkomsten)+1
        kolomBreedte = max(20, kolomBreedte)
        #De antwoorden zijn niet getallen; druk een lijst af
        print('\n'+antwoorduitleg)
        for regel in range(1, mini and 7 or 13):
            volgendeRegel = regel+(mini and 6 or 12)
            print('%2d: %-*s %2d: %s'%(
            regel, kolomBreedte, uitkomsten[regel-1], volgendeRegel, uitkomsten[volgendeRegel-1]))
    print('\nOplossing:')
    print(repr(bord))

#------------------routines voor het genereren van oefenmateriaal-----
def randrangeMod24(low=1, high=121):
    '''Genereer randomgetallen uit de range
    die respectievelijk op de verschillende vakjes
    van het bord uitkomen.
    (Dus, van 1 mod 24, 2 mod 24, ... 24 mod 24.)
    >>> list(randrangeMod24(10, 34))
    [25, 26, 27, 28, 29, 30, 31, 32, 33, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24]
    '''
    for rest in range(1,25):
        #Berekening: schuif interval rest naar links
        #deel grenzen door 24, naar boven afgerond
        ondergrens24 = (low-1-rest)//24+1
        bovengrens24 = (high-1-rest)//24+1
        yield randrange(ondergrens24, bovengrens24)*24+rest

def gcd(a,b):
    """Greatest common divisor.
    >>> gcd(4,10)
    2
    >>> gcd(-5,0)
    5
    >>> gcd(0,1)
    1
    """
    while b: a,b = b, a%b
    return abs(a)

def showFraction(t, n, gehelen = True):
    """Geef een breuk op een mooie manier weer.
    Vereenvoudigt de breuk ook nog.
    Zet spaties voor om een goede sorteervolgorde te maken.
    Geeft als tweede resultaat de floating point waarde van de breuk.
    >>> showFraction(1,3)[0]
    '   1/3'
    >>> showFraction(4,3,False)[0]
    '   4/3'
    >>> showFraction(4,3)[0]
    ' 1 1/3'
    >>> showFraction(40,3)[0]
    '13 1/3'
    >>> showFraction(6,3)
    (' 2', 2.0)
    """
    floatvalue = 1.0*t/n
    #vereenvoudig antwoord
    g = gcd(t,n)
    t, n = t//g, n//g
    #haal gehelen eruit (als nodig)
    if gehelen: g, t = divmod(t, n)
    else: g = 0
    #maak mooie string
    if t==0:
        #0/n achter de gehelen
        return '%2d'%g, floatvalue
    elif n==1:
        #t/1 als breuk (zonder gehelen)
        return '%2d'%t, floatvalue
    elif g==0:
        #0 als geheel (drie spaties voor sortering
        return '   %d/%d'%(t,n), floatvalue
    else:
        #overig
        return '%2d %d/%d'%(g,t,n), floatvalue

#combinatie van uren- en minutenwoorden
getalwoorden='twaalf een twee drie vier vijf zes zeven acht '\
    'negen tien elf twaalf dertien veertien kwart'.split()
def spreekTijd(uren, minuten):
    """Spreek een tijd uit.
    >>> spreekTijd(2,0)
    'twee uur'
    >>> spreekTijd(4,31)
    'een minuut over half vijf'
    >>> spreekTijd(18,45)
    'kwart voor zeven'
    >>> spreekTijd(12,30)
    'half een'
    """
    if minuten==0:
        return getalwoorden[uren%12]+' uur'
    elif 0<minuten<=15:
        tussenvoegsel = ' over '
    elif 15<minuten<30:
        tussenvoegsel = ' voor half '
        uren += 1
        minuten = 30-minuten
    elif minuten==30:
        return 'half '+getalwoorden[uren%12+1]
    elif 30<minuten<45:
        tussenvoegsel = ' over half '
        uren += 1
        minuten -= 30
    elif 45<=minuten<60:
        tussenvoegsel = ' voor '
        uren += 1
        minuten = 60-minuten
    else: raise ValueError("aantal minuten klopt niet")
    if minuten==1: minString = " minuut"
    elif minuten%5==0: minString = ""
    else: minString = " minuten"
    return getalwoorden[minuten]+minString+tussenvoegsel+getalwoorden[uren%12]

#------------onderwerpen
onderwerpenLijst = []
def onderwerp(onderwerpFunctie):
    onderwerpenLijst.append(onderwerpFunctie.__name__)
    return onderwerpFunctie

#moeilijksgraden lopen van 0 tot en zonder 5

@onderwerp
def delingen(moeilijkheidsgraad=4, miniloco=False):
    """delingen
    quotient van 2 cijfers
    4..39 tot en met 12..114"""
    assert not miniloco, "Miniloco niet toegestaan bij dit onderwerp."
    quotientL = 2+moeilijkheidsgraad*2
    quotientH = 40+moeilijkheidsgraad*15
    delerL = 10
    delerH = quotientH
    opgaven = []
    for quotient in randrangeMod24(quotientL, quotientH):
        deler = randrange(delerL, delerH)
        opgaven.append(('%d/%d'%(deler*quotient,deler), quotient))
    return opgaven


@onderwerp
def vermenigvuldigingen(moeilijkheidsgraad=4, miniloco=False):
    """vermenigvuldigingen
    factoren tot maximaal 9..14
    """
    assert not miniloco, "Miniloco niet toegestaan bij dit onderwerp."
    low = 2+moeilijkheidsgraad
    high = 10+moeilijkheidsgraad
    opgaven = []
    for i in range(40):
        x = randrange(low, high)
        y = randrange(low, high)
        opgaven.append(('%d*%d'%(x,y), x*y))
    return opgaven


@onderwerp
def moeilijkeVermenigvuldigingen(moeilijkheidsgraad=4, miniloco=False):
    '''Vermenigvuldigen; laatste cijfer eraf
    Factoren tot maximaal 19..44'''
    assert not miniloco, "Miniloco niet toegestaan bij dit onderwerp."
    low = 10
    high = 20+moeilijkheidsgraad*5
    #doe er lekker veel om de afstanden te verlagen
    #24*log(24)=76
    opgaven = []
    for i in range(100):
        x = randrange(low, high)
        y = randrange(low, high)
        opgaven.append(('%d*%d'%(x,y), x*y//10))
    return opgaven


@onderwerp
def aftrekkingen(moeilijkheidsgraad=4, miniloco=False):
    """aftrekkingen
    uitkomst 1..19 tot 6..119
    """
    assert not miniloco, "Miniloco niet toegestaan bij dit onderwerp."
    verschilL = 1+moeilijkheidsgraad
    verschilH = 20+moeilijkheidsgraad*20
    aftrekkerL = 10+moeilijkheidsgraad*25
    aftrekkerH = 100+moeilijkheidsgraad*225
    opgaven = []
    for verschil in randrangeMod24(verschilL, verschilH):
        aftrekker = randrange(aftrekkerL, aftrekkerH)
        opgaven.append(('%d-%d'%(aftrekker+verschil,aftrekker), verschil))
    return opgaven


@onderwerp
def miniOptellingen(moeilijkheidsgraad=2, miniloco=True):
    '''optellingen met miniloco
    Maak simpele optellingen in MiniLoco.
    Moet worden uitgebreid om Hanna te helpen.
    >>> seed(4)
    >>> miniOptellingen()
    ['9-8', '10-8', '7-4', '10-6', '11-6', '7-1', '0+7', '2+6', '2+7', '9+1', '5+6', '9+3']
    '''
    assert miniloco, "Miniloco verplicht bij dit onderwerp."
    maximumWaarde = 5+moeilijkheidsgraad*3
    opgaven=[]
    for som in range(1,13):
        term = randrange(min(0,som-maximumWaarde),som+1)
        opgaven.append('%d%+d'%(som-term, term))
    return opgaven


#Maak een scheve kansverdeling voor de tafels
tafelTabel = {
    0: [0, 1, 2, 2, 3, 3, 5, 5, 10],
    1: [4, 4],
    2: [6, 6, 3, 4, 6],
    3: [8, 8, 8],
    4: [9, 9, 9],
    5: [7, 7, 7, 7, 9]}

@onderwerp
def tafels(moeilijkheidsgraad=0, miniloco=False):
    """vermenigvuldigtafels oefenen
    Moeilijkheidsgraad bepaalt aantal tafeltjes van 0,1,2,3,5,10 (1)
    tot allemaal (5)
    Vermenigvuldig een aantal tafeltjes.
    Voor Hanna, juni 07.
    >>> seed(20)
    >>> printFormulier(tafels())
    Opgaven (blokjes):
     1: 4 x 5=               13: 8 x 1=
     2: 8 x 3=               14: 6 x 2=
     3: 3 x 3=               15: 8 x 2=
     4: 4 x 3+1=             16: 5 x 5=
     5: 7 x 10=              17: 3 x 2=
     6: 2 x 5+1=             18: 3 x 5=
     7: 6 x 3=               19: 8 x 5+1=
     8: 7 x 2=               20: 2 x 1=
     9: 1 x 10=              21: 2 x 2=
    10: 9 x 5=               22: 5 x 10-3=
    11: 7 x 1=               23: 2 x 10-1=
    12: 1 x 5=               24: 9 x 3=
    <BLANKLINE>
    Oplossing:
    GG\ \GGGR/    \R\   \BBB
    GGG\ \GG/      \B\   \BB
    GGG/  \G\      /BB\   \B
    GG/    \R\    /RBBB\   \\
    G/     /RRR/\RRRBBB/   /
    /     /GRR/  \RRBB/   /B
    \    /GGRR\  /RRB/   /BB
    G\  /GGGRRR\/RRR/   /BBB
    """
    assert not miniloco, "Miniloco niet toegestaan bij dit onderwerp."
    tafels = []
    for i in range(moeilijkheidsgraad+1):
        tafels.extend(tafelTabel[i])
    opgaven = {}
    while len(opgaven)<40:
        x = randrange(1,10)
        y = choice(tafels)
        opgave = '%d x %d'%(x,y)
        opgaven[opgave] = x*y
    return list(opgaven.items())

@onderwerp
def miniVermenigvuldigTafels(moeilijkheidsgraad=2, miniloco=True):
    """vermenigvuldigen met miniloco met gegeven tafels(lijst)
    Vermenigvuldigingen uit beperkt aantal tafels
    met miniLoco en antwoordenlijst.
    Voor ongeduldige Hanna, juni 07.
    """
    assert miniloco, "Miniloco verplicht bij dit onderwerp."
    tafels = []
    for i in range(moeilijkheidsgraad+1):
        tafels.extend(tafelTabel[i])
    opgaven = {}
    while len(opgaven)<12:
        x=randrange(1,11)
        y=choice(tafels)
        result = '%3d'%(x*y)
        opgaven[result] = '%d x %d'%(x,y)
    return [(o,r) for r,o in list(opgaven.items())]


@onderwerp
def getalMaalBreuk(moeilijkheidsgraad=3, miniloco=False):
    """Getal maal breuk
    moeilijkheidsgraad bepaalt van alles
    voor Tessa, juni 07.
    >>> seed(3)
    >>> b=getalMaalBreuk()
    >>> b.sort(key = itemgetter(2))
    >>> for o,r,w in b:
    ...   print(r.ljust(6), '=', o)
       1/3 = 1 x (2/6)
       3/7 = 1 x (3/7)
       4/7 = 1 x (4/7)
     1     = 1 x (3/3)
     1 1/5 = 2 x (3/5)
     1 2/3 = 2 x (5/6)
     1 3/4 = 7 x (2/8)
     2 4/7 = 3 x (6/7)
     3     = 4 x (3/4)
     3 1/3 = 5 x (4/6)
     3 3/7 = 4 x (6/7)
     3 1/2 = 7 x (2/4)
     3 4/7 = 5 x (5/7)
     3 3/4 = 5 x (6/8)
     4 1/6 = 5 x (5/6)
     4 1/5 = 7 x (3/5)
     4 1/2 = 3 x (3/2)
     5     = 8 x (5/8)
     5 3/5 = 7 x (4/5)
     6     = 8 x (3/4)
     6 2/5 = 8 x (4/5)
     6 6/7 = 8 x (6/7)
     7     = 7 x (4/4)
     8     = 6 x (4/3)
    """
    assert not miniloco, "Miniloco niet toegestaan bij dit onderwerp."
    #Maak een dictionary op uitkomst om dubbele te voorkomen
    opgaven = {}
    while len(opgaven)<(miniloco and 12 or 24):
        x = randrange(1,moeilijkheidsgraad+6)
        n = randrange(2,moeilijkheidsgraad+6)
        if moeilijkheidsgraad>3:
            while n==x:
                n = randrange(1,moeilijkheidsgraad+6)
        if moeilijkheidsgraad>3:
            t = randrange(2, max(10,2*n))
            #zorg dat t geen veelvoud van n is
            t += t//(n-1)
        else:
            t = randrange(2,max(5,n))
        r,w = showFraction(x*t, n, moeilijkheidsgraad>2)
        opgaven[r] = ('%d x (%d/%d)'%(x,t,n), w)
    return [(o,r,w) for r,(o,w) in list(opgaven.items())]


@onderwerp
def breukenOptellen(moeilijkheidsgraad=4, miniloco=False):
    """breuken optellen
    Breuken optellen met vereenvoudigen, noemer antwoord onder de 20.
    Neem de optelling a/b+c/d, met vereenvoudigde breuken,
    dus gcd(a,b)==gcd(c,d)==1
    Als s de gcd is van b en d, kunnen we dit schrijven als
    a/(b*s)+c/(d*s)
    met gcd(a,b)==gcd(c,d)==gcd(b,d)==gcd(a,s)==gcd(c,s)==1
    (met andere woorden: alleen b en d kunnen nog een factor s hebben).
    Het resultaat is dan (a*d*s+b*c*s)/(b*d*s*s) = (a*d+b*c)/(b*d*s)
    en dit kan verder worden vereenvoudigd als a*d+b*c==0 (mod s).
    In dit geval kunnen b en d dus geen factor s hebben,
    want dan zouden ze er allebei een hebben, en dat kan niet.
    We kiezen dus gcd(b,s)==gcd(d,s)==1 en d = -b*c/a (mod s).

    Hoe genereer je zo'n "interessante" optelling?
    Kies een s.
    Kies a,b,c,d zo dat gcd(a,s)==gcd(b,s)==gcd(c,s)==gcd(d,s)==1
    en bovendien gcd(a,b)==gcd(c,d)==gcd(b,d)==1
    Verder moet voor het gemak 1<=a,b,c,d<=10.
    De keuze is beperkt:
    als s==2 kan je voor a,b,c,d kiezen uit 1,3,5,7,9,
    maar je mag 3 en 9 alleen allebei gebruiken als het a en c betreft.
    als s==3 kan je voor a,b,c,d kiezen uit 1,5,7,
    en hebben we beperkte keuze uit 2,4,8,10.
    
    Conclusie: we genereren maar wat, en we hopen maar dat er een leuke langskomt.
    De tekst boven is dan een inspiratie voor eventuele uitbreidingen.
    >>> seed(27)
    >>> printFormulier(breukenOptellen(5))
    Opgaven (blokjes):
     1: 7/12 + 5/6           13: 1/12 + 3/10
     2: 5/8 + 1/12           14: 5/12 + 8/9
     3: 1 + 3/4 - 11/12      15: 1/4 + 3/8
     4: 6/7 + 1/2            16: 0/1 + 1/5
     5: 7/12 + 3/8           17: 5/6 + 1/6
     6: 8/9 + 1/12           18: 3/4 + 3/4
     7: 5/8 - 1/2            19: 5/7 + 1/2
     8: 4/9 + 3/10           20: 2/5 + 3/4
     9: 3/8 + 5/6            21: 7/8 + 3/4
    10: 0/1 + 7/12           22: 8/9 - 5/6
    11: 2/5 + 1/3            23: 1/6 + 8/9
    12: 1 + 1/6 - 1/2        24: 4/7 + 7/11
    <BLANKLINE>
    Uitkomsten (vakjes):
     1:    1/18              13:    35/36
     2:    1/8               14:  1
     3:    1/5               15:  1 1/18
     4:    23/60             16:  1 3/20
     5:    7/12              17:  1 16/77
     6:    5/8               18:  1 5/24
     7:    2/3               19:  1 3/14
     8:    17/24             20:  1 11/36
     9:    11/15             21:  1 5/14
    10:    67/90             22:  1 5/12
    11:    5/6               23:  1 1/2
    12:    23/24             24:  1 5/8
    <BLANKLINE>
    Oplossing:
    BB\    /GGG/\GGG\    /RR
    BBB\  /BGG/  \GGR\  /RRR
    BBB/ /BBGG\  /GGRR\ \RRR
    BB/ /BBBGGG\/GGGRRR\ \RR
    B/  \BBBG/    \GRRR/  \R
    /    \BB/      \RR/    \\
    \     \B\      /R/     /
    B\     \G\    /G/     /R
    """
    #maak dictionary op uitkomst om dubbele te voorkomen
    opgaven = {}
    while len(opgaven)<(miniloco and 12 or 24):
        b = randrange(1,8+moeilijkheidsgraad)
        a = 0
        while gcd(a,b)>1: a = randrange(1,b)
        d = randrange(2,8+moeilijkheidsgraad)
        c = 0
        while gcd(c,d)>1: c = randrange(1,d)
        if gcd(a*d+b*c,b*d)==1:
            #som heeft geen interessante vereenvoudiging
            #laat dit soms toe, als b*d niet al te groot
            if b*d>100 or randrange(3): continue
            o,(r,w) = '%d/%d + %d/%d'%(a,b,c,d), showFraction(a*d+b*c, b*d)
        elif randrange(3):
            #optelling met vereenvoudiging
            o,(r,w) = '%d/%d + %d/%d'%(a,b,c,d), showFraction(a*d+b*c, b*d)
        elif a*d>b*c:
            #af en toe een aftrekking, resultaat is positief
            o,(r,w) = '%d/%d - %d/%d'%(a,b,c,d), showFraction(a*d-b*c, b*d)
        else:
            #aftrekking met negatief resultaat
            o,(r,w) = '1 + %d/%d - %d/%d'%(a,b,c,d), showFraction(b*d+a*d-b*c, b*d)
        opgaven[r] = o,w
    return [(o,r,w) for r,(o,w) in list(opgaven.items())]


@onderwerp
def klokKijken(moeilijkheidsgraad=3, miniloco=False):
    """digitaal naar analoog
    >>> seed(12)
    >>> printFormulier(klokKijken())
    Opgaven (blokjes):
     1: 20:00                13: 11:35
     2: 16:40                14: 15:05
     3: 16:35                15: 13:35
     4: 23:55                16: 12:05
     5:  6:45                17:  1:45
     6: 17:55                18: 14:05
     7:  9:00                19:  1:50
     8:  3:50                20:  2:25
     9: 16:00                21:  0:20
    10:  8:20                22:  9:25
    11:  4:45                23:  3:15
    12:  3:25                24:  1:40
    <BLANKLINE>
    Uitkomsten (vakjes):
     1: tien voor half een     13: vijf voor half tien
     2: tien over half twee    14: vijf over half twaalf
     3: kwart voor twee        15: vijf over twaalf
     4: tien voor twee         16: vijf over half twee
     5: vijf voor half drie    17: vijf over twee
     6: kwart over drie        18: vijf over drie
     7: vijf voor half vier    19: vier uur
     8: tien voor vier         20: vijf over half vijf
     9: kwart voor vijf        21: tien over half vijf
    10: kwart voor zeven       22: vijf voor zes
    11: tien voor half negen   23: acht uur
    12: negen uur              24: vijf voor twaalf
    <BLANKLINE>
    Oplossing:
    \RRRRRR/\BBBBBB/\GGGGGG/
     \RRRR/  \BBBB/  \GGGG/ 
    \      /\      /\      /
    B\    /GG\    /RR\    /B
    B/    \GG/    \RR/    \B
    /      \/      \/      \\
     /RRRR\  /BBBB\  /GGGG\ 
    /RRRRRR\/BBBBBB\/GGGGGG\\
    """
    minutenFactor = {0: 60, 1: 30, 2: 15, 3: 5, 4: 1, 5:1}[moeilijkheidsgraad]
    result = []
    antwoorden = set()
    if moeilijkheidsgraad<2: miniloco = True
    while len(antwoorden)<(miniloco and 12 or 24):
        uren, minuten = randrange(24), randrange(0, 60, minutenFactor)
        opgave = '%2d:%02d'%(uren, minuten)
        antwoord = spreekTijd(uren, minuten)
        if antwoord not in antwoorden:
            result.append((opgave, antwoord, opgave))
            antwoorden.add(antwoord)
    return result

@onderwerp
def hoofdsteden(moeilijkheidsgraad=0, miniLoco=True):
    """De hoofdsteden van Europa.
    >>> seed(19)
    >>> printFormulier(hoofdsteden(1))
    Opgaven (blokjes):
     1: Finland               7: Denemarken
     2: Ierland               8: Spanje
     3: Oekra\xc3\xafne              9: Belgi\xc3\xab
     4: Zweden               10: Duitsland
     5: Zwitserland          11: Verenigd Koninkrijk
     6: Oostenrijk           12: Noorwegen
    <BLANKLINE>
    Uitkomsten (vakjes):
     1: Dublin                7: Madrid
     2: Wenen                 8: Oslo
     3: Helsinki              9: Berlijn
     4: Stockholm            10: Kopenhagen
     5: Brussel              11: Londen
     6: Kiev                 12: Bern
    <BLANKLINE>
    Oplossing:
     /BBBB\  /GGGG\  /RRRR\ 
    /BBBBBB\/GGGGGG\/RRRRRR\\
    \BBBBBB/\GGGGGG/\RRRRRR/
     \BBBB/  \GGGG/  \RRRR/ 
    >>> printFormulier(hoofdsteden(2))
    Opgaven (blokjes):
     1: Zwitserland           7: Noorwegen
     2: Rusland               8: Finland
     3: Portugal              9: Tsjechi\xc3\xab
     4: Denemarken           10: Spanje
     5: Oostenrijk           11: Belgi\xc3\xab
     6: Ierland              12: Zweden
    <BLANKLINE>
    Uitkomsten (vakjes):
     1: Praag                 7: Wenen
     2: Lissabon              8: Brussel
     3: Moskou                9: Dublin
     4: Helsinki             10: Stockholm
     5: Kopenhagen           11: Madrid
     6: Bern                 12: Oslo
    <BLANKLINE>
    Oplossing:
    RR\  /RRBBB/BB\  /GGGG\ 
    RRR\/RRRBB/ BBB\/GGGGGG\\
    \RRRRRR/\BBB /BBGGG/\GGG
     \RRRR/  \BB/BBBGG/  \GG
    """
    assert miniLoco, "Hoofdsteden moet met miniLoco"
    #c3ab is e with dots, c3af is i with dots
    leerstof = b"""
        Belgi\xc3\xab   Brussel         2
        Denemarken      Kopenhagen      2
        Duitsland       Berlijn         1
        Verenigd-Koninkrijk Londen      1
        Finland         Helsinki        2
        Frankrijk       Parijs          1
        Ierland         Dublin          2
        Itali\xc3\xab   Rome            1
        Nederland       Amsterdam       0
        Noorwegen       Oslo            2
        Oekra\xc3\xafne Kiev            1
        Oostenrijk      Wenen           2
        Portugal        Lissabon        2
        Rusland         Moskou          2
        Spanje          Madrid          2
        Tsjechi\xc3\xab Praag           2
        Zweden          Stockholm       2
        Zwitserland     Bern            2
    """.decode('utf8')
    #maak paren voor de relevante landen
    paren = []
    for line in leerstof.splitlines():
        if not line.strip(): continue
        land, hoofdstad, niveau = line.split()
        if int(niveau)<moeilijkheidsgraad: continue
        land = land.replace("-", " ")
        paren.append((land, hoofdstad))
    shuffle(paren)
    return [(paren[i][0], paren[i][1], i)
        for i in range(12)]

#----Main program
import sys
def usage(error=1):
    print("Beschikbare onderwerpen:")
    for o in onderwerpenLijst:
        print(o, globals()[o].__doc__.splitlines()[0])
    print("\nGeef %s [+-]<onderwerp>[<moeilijkheidsgraad>]"%sys.argv[0])
    print("Moeilijkheidsgraad loopt van 0 tot 5")
    print("+: forceer maxiloco; -: forceer miniloco")
    print("Eerste letters van onderwerp is genoeg")
    sys.exit(error)

if __name__=='__main__':
    if len(sys.argv)==1:
        #geen argumenten: test
        print("Even mezelf testen...")
        import doctest, loco
        if doctest.testmod()[0]!=0:
            sys.exit(2)
        else:
            print('Programma is getest.')
            usage(0)

    for onderwerp in sys.argv[1:]:
        #argumenten voor de opgavenfunctie
        argumenten = {}
        #haal minilocovlag eraf
        if onderwerp[0] in '+-':
            argumenten['miniloco'] = onderwerp[0]=='-'
            onderwerp = onderwerp[1:]
        #splits onderwerp en argumenten
        if onderwerp[-1].isdigit():
            argumenten['moeilijkheidsgraad'] = int(onderwerp[-1])
            onderwerp = onderwerp[:-1]
        #zoek in onderwerpenLijst naar matchend onderwerp
        mogelijkeOnderwerpen = [
            o
            for o in onderwerpenLijst
            if o.lower().startswith(onderwerp.lower())]
        #mooie foutmelding
        if len(mogelijkeOnderwerpen)!=1:
            print("Onbekend onderwerp:", onderwerp)
            usage()
        onderwerp = mogelijkeOnderwerpen[0]
        #actie
        print("Onderwerp:", onderwerp)
        if onderwerp=="moeilijkeVermenigvuldigingen":
            uitleg = "Laat laatste cijfer van het product weg:"
        else:
            uitleg = "Opgaven (blokjes):"
        for i in range(4):
            printFormulier(globals()[onderwerp](**argumenten), uitleg)
            print('_'*40)
            print()
        print()
